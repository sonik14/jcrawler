package nl.pointpro.jcrawler.util;

public enum IterateAction {
  CONTINUE,
  REMOVE,
  RETURN,
  REMOVE_AND_RETURN
}
