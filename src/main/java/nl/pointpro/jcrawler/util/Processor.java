package nl.pointpro.jcrawler.util;

public abstract class Processor<T, R> {
  public abstract R apply(T arg);
}
