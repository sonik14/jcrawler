/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.pointpro.jcrawler.frontier;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import nl.pointpro.jcrawler.crawler.Configurable;
import nl.pointpro.jcrawler.crawler.CrawlConfig;
import nl.pointpro.jcrawler.crawler.WebCrawler;
import nl.pointpro.jcrawler.crawler.exceptions.QueueException;
import nl.pointpro.jcrawler.fetcher.PageFetcher;
import nl.pointpro.jcrawler.url.WebURL;
import nl.pointpro.jcrawler.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sleepycat.je.Environment;

/**
 * @author Yasser Ganjisaffar
 */

public class Frontier extends Configurable
{
  protected static final Logger logger = LoggerFactory.getLogger(Frontier.class);
  
  /** Identifier for the InProgress queue: pages in progress by a thread */
  public static final int IN_PROGRESS_QUEUE = 1;
  /** Identifier for the WorkQueue: pages not yet claimed by any thread */
  public static final int WORK_QUEUE = 2;
  /** convenience identifier for both queues: IN_PROGRESS_QUEUE | WORK_QUEUE */
  public static final int BOTH_QUEUES = IN_PROGRESS_QUEUE | WORK_QUEUE;

  protected CrawlQueue queue;

  protected final Object mutex = new Object();
  protected final Object waitingList = new Object();

  protected boolean isFinished = false;
  protected long lastSleepNotification = 0;

  protected long scheduledPages;

  protected DocIDServer docIdServer;
  
  protected Counters counters;
  
  /** An ordered list of the top of the work queue, sorted by priority and docid */
  protected Set<WebURL> current_queue = new TreeSet<WebURL>();
  
  /** A list of seeds that have finished, and so their offspring should be skipped */
  protected Set<Long> finished_seeds = new HashSet<Long>();

  public Frontier(Environment env, CrawlConfig config, DocIDServer docIdServer) {
    super(config);
    this.counters = new Counters(env, config);
    this.docIdServer = docIdServer;
    this.queue = new BerkeleyDBQueue(env, config);
    
    scheduledPages = counters.getValue(Counters.ReservedCounterNames.SCHEDULED_PAGES);
  }

  /**
   * Schedule a list of URLs at once, trying to minimize synchronization overhead.
   * 
   * @param urls The list of URLs to schedule
   */
  public void scheduleAll(Collection<WebURL> urls) {
    synchronized (mutex) {
      List<WebURL> accepted = new ArrayList<WebURL>();
      for (WebURL url : urls) {
        if (!url.isHttp()) {
          logger.warn("Not scheduling URL {} - Protocol {} not supported", url.getURL(), url.getProtocol());
          continue;
        }
        
        if (finished_seeds.contains(url.getSeedDocid())) {
          logger.warn("Not scheduling {} - its seed is marked as finished", url);
          continue;
        }
        
        if (url.getDocid() < 0) {
          long docid = this.docIdServer.getNewUnseenDocID(url.getURL());
          if (docid == -1)
            continue;
          url.setDocid(docid);
        }
        
        if (url.getSeedDocid() < 0)
          throw new RuntimeException("SeedDocid is not set for URL " + url.getURL());
        
        accepted.add(url);
      }
      
      List<WebURL> queue_rejects = queue.enqueue(accepted);
      scheduledPages += (accepted.size() - queue_rejects.size());
      
      if (queue_rejects.size() > 0)
        logger.error("Could not add all URLs to the list: {}/{} were rejected", accepted.size(), queue_rejects.size());
    }
    
    counters.setValue(Counters.ReservedCounterNames.SCHEDULED_PAGES, scheduledPages);
    
    synchronized (waitingList) {
      waitingList.notify();
    }
  }
  
  /**
   * Put a new URL that is not a seed in the queue.
   * 
   * @see Frontier#schedule(WebURL, boolean)
   * 
   * @param url The URL to schedule
   * @return True when enqueued, false otherwise
   */
  public boolean schedule(WebURL url) {
    return schedule(url, false);
  }
  /**
   * Method that actually puts a new URL in the queue. It checks
   * the DocID. If it is -1, it is assumed that this is a newly discovered URL 
   * that should be crawled. If it has already been seen, it is skipped.
   * 
   * @param url The WebURL to schedule
   * @param isSeed Whether the URL is a new seed
   * @return True if the URL was added to the queue, false otherwise.
   */
  public boolean schedule(WebURL url, boolean isSeed) {
    synchronized (mutex) {
      if (!url.isHttp()) {
        logger.warn("Not scheduling URL {} - Protocol {} not supported", url.getURL(), url.getProtocol());
        return false;
      }

      if (finished_seeds.contains(url.getSeedDocid())) {
        logger.warn("Not scheduling {} - its seed is marked as finished", url);
        return false;
      }
      
      if (url.getDocid() < 0) {
        long docid = this.docIdServer.getNewUnseenDocID(url.getURL());
        if (docid == -1)
          return false;
        url.setDocid(docid);
      }
      
      if (isSeed) {
        url.setSeedDocid(url.getDocid());
        url.setParentDocid(-1);
      }

      // A URL without a seed doc ID is a seed of itself.
      if (url.getSeedDocid() < 0)
        throw new RuntimeException("SeedDocid is not set for URL " + url.getURL());

      try {
        queue.enqueue(url);
        ++scheduledPages;
        counters.increment(Counters.ReservedCounterNames.SCHEDULED_PAGES);
      } catch (RuntimeException e) {
        logger.error("Error while putting the url in the work queue", e);
        return false;
      }

      // Wake up threads
      synchronized (waitingList) {
        waitingList.notifyAll();
      }

      return true;
    }
  }
  
  /**
   * Remove all document IDs from the DocIDServer. This allows to re-crawl
   * pages that have been visited before, which can be useful in a long-running
   * crawler that may revisit pages after a certain amount of time.
   * 
   * This method will wait until all queues are empty to avoid purging DocIDs
   * that are still will be crawled before actually clearing the database, so make
   * sure the crawler is running when executing this method.
   */
  public void clearDocIDs() {
    while (true) {
      if (getQueueLength() > 0) {
        synchronized (waitingList) {
          try {
            waitingList.wait(2000);
          } catch (InterruptedException e)
          {}
        }
      } else {
        synchronized (mutex) {
          if (getQueueLength() > 0)
            continue;
          docIdServer.clear();
          logger.info("Document ID Server has been emptied.");
          break;
        }
      }
    }
  }
  
  /** 
   * Remove a specific URL from the DocIDServer so that it can be crawled again.
   * 
   * @param url The URL to forget
   */
  public void forgetURL(String url) {
    docIdServer.forget(url);
  }
  
  /**
   * Add a seed docid that has finished. This is used to determine
   * whether upcoming URLs still need to be crawled. This could be
   * used to abort a seed when it has finished to waste as little time
   * on it as possible.
   * 
   * If the seed doc ID has no offspring in the queue, nothing happens.
   * 
   * @param seed_doc_id The docid of the seed URL to mark as finished.
   */
  public void setSeedFinished(long seed_doc_id) {
    // Nothing to do if it's already marked as finished
    if (finished_seeds.contains(seed_doc_id))
      return;
    
    synchronized (mutex) {
      finished_seeds.add(seed_doc_id);
      queue.removeOffspring(seed_doc_id);
    }
  }
  
  public WebURL getNextURL(WebCrawler crawler, PageFetcher pageFetcher) throws QueueException
  {
    while (true)
    {
      WebURL url;
      synchronized (mutex) {
        // Handle one ended seed if there are any
        Iterator<Long> iter = finished_seeds.iterator();
        
        while (iter.hasNext()) {
          long finished_seed = iter.next();
          long num_offspring = queue.getNumOffspring(finished_seed);
          // Only call handleSeedEnd when there is no more offspring
          if (num_offspring == 0) {
            logger.debug("Seed {} is marked as finished and has no remaining offspring - calling WebCrawler#handleSeedEnd on {}", finished_seed, crawler);
            crawler.handleSeedEnd(finished_seed);
            iter.remove();
            break;
          } else {
            logger.debug("Seed {} is marked as finished but has {} offspring in queue - postponing calling WebCrawler#handleSeedEnd", finished_seed, num_offspring);
          }
        }
        
        url = queue.getNextURL(crawler, pageFetcher);
        
        if (url != null && finished_seeds.contains(url.getSeedDocid())) {
          // No need to hand out this URL again, it's already finished
          logger.debug("Skipping {} as its seed is finished", url);
          queue.setFinishedURL(crawler, url);
          continue;
        }
      }
      
      if (url == null) {
        if (queue.getQueueSize() > 0) {
          // Track politeness wait
          pageFetcher.doPolitenessWait(waitingList, config.getPolitenessDelay());
        } else {
          // Just wait for a while to see if more URLs come in, but don't
          // track as an empty queue is not really 'politeness delay'
          Util.wait(waitingList, config.getPolitenessDelay());
        }
        
        continue;
      }
      
      // Proper URL found, go crawl it!
      return url;
    }
  }

  /**
   * Set the page as processed.
   * 
   * @param crawler The crawler that has processed the URL
   * @param webURL The URL to set as processed
   * @throws QueueException When webURL was not assigned to crawler.
   */
  public void setProcessed(WebCrawler crawler, WebURL webURL) throws QueueException {
    counters.increment(Counters.ReservedCounterNames.PROCESSED_PAGES);
    synchronized (mutex) {
      queue.setFinishedURL(crawler, webURL);
      if (queue.getNumOffspring(webURL.getSeedDocid()) == 0) {
        logger.info("{} finished {} - seed has no offspring left - marking {} as finished", crawler, webURL, webURL.getSeedDocid());
        finished_seeds.add(webURL.getSeedDocid());
      }
    }
  }

  public long numOffspring(Long seedDocid) {
    synchronized (mutex) {
      return queue.getNumOffspring(seedDocid);
    }
  }
  
  public long getQueueLength() {
    synchronized (mutex) {
      return queue.getQueueSize();
    }
  }

  public long getNumberOfAssignedPages() {
    synchronized (mutex) {
      return queue.getNumInProgress();
    }
  }

  public long getNumberOfProcessedPages() {
    return counters.getValue(Counters.ReservedCounterNames.PROCESSED_PAGES);
  }

  public boolean isFinished() {
    return isFinished;
  }

  public void close() {
    counters.close();
  }

  public void finish() {
    isFinished = true;
    synchronized (waitingList) {
      waitingList.notifyAll();
    }
  }

  /**
   * Allow a certain piece of code to be run synchronously. This method
   * acquires the mutex and then runs the run method in the provided runnable.
   * 
   * @param r The object on which to run the run method synchronized
   */
  public void runSync(Runnable r) {
      synchronized (mutex) {
          r.run();
      }
  }

  public void reassign(Thread oldthread, Thread newthread) {
    synchronized (mutex) {
      WebURL url = queue.reassign(oldthread, newthread);
      if (url != null)
        logger.info("Reassigning URL {} from crawl thread {} to crawl thread {}", url.getURL(), oldthread.getId(), newthread.getId());
    }
  }
}
